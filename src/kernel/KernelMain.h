extern int LockTest();
extern int TcpTest();
extern int Experiments();
extern int InitProcess();

static void KernelMain() {
#if TESTING_LOCK_TEST
  LockTest();
#endif
#if TESTING_TCP_TEST
  TcpTest();
#endif
  Experiments();
  InitProcess();
}
