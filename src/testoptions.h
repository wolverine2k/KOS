// **** general options - testing

#define TESTING_ENABLE_ASSERTIONS     1
#define TESTING_ENABLE_STATISTICS     1
#define TESTING_ENABLE_DEBUGGING      1
#define TESTING_ALWAYS_MIGRATE        1 // KOS only
#define TESTING_DISABLE_HEAP_CACHE    1 // KOS only

// **** general options - safer execution

//#define TESTING_DISABLE_ALLOC_LAZY    1 // KOS only
//#define TESTING_DISABLE_DEEP_IDLE     1 // KOS only
//#define TESTING_DISABLE_PREEMPTION    1 // KOS only
//#define TESTING_REPORT_INTERRUPTS     1 // KOS only

// **** general options - alternative design

#define TESTING_LOADBALANCING         1 // enable load balancing using ISRS
//#define TESTING_OPTIMISTIC_ISRS       1 // search fibre first, correct later for ISRS
//#define TESTING_SHARED_READYQUEUE     1 // use shared ready queue, instead of stealing
//#define TESTING_LOCKED_READYQUEUE     1 // locked vs. lock-free ready queue
#define TESTING_NEMESIS_READYQUEUE    1 // lock-free: nemesis vs. stub-based MPSC
//#define TESTING_PLACEMENT_STAGING     1 // load-based staging vs. round-robin placement
//#define TESTING_IDLE_SPIN         65536 // spin before idle loop
#define TESTING_HALT_SPIN         65536 // spin before halting worker pthread
//#define TESTING_MUTEX_FIFO            1 // use fifo/baton mutex
//#define TESTING_MUTEX_BARGING         1 // use blocking/barging mutex
//#define TESTING_MUTEX_SPIN            1 // spin before block in non-fifo mutex

// **** libfibre options - event handling

//#define TESTING_PROCESSOR_POLLER      1 // poll events directly from each worker
#define TESTING_CLUSTER_POLLER_FIBRE  1 // per-cluster poller: fibre vs. pthread
#define TESTING_POLLER_FIBRE_SPIN 65536 // poller fibre: spin loop of NB polls
#define TESTING_LAZY_FD_REGISTRATION  1 // lazy vs. eager registration after fd creation
//#define TESTING_IOYIELD_CONDITIONAL   1 // yield-before-read only when FD not registered for poll
//#define TESTING_TRY_IO_BEFORE_YIELD   1 // make one non-blocking I/O attempt before yield

// **** KOS console/serial output configuration

//#define TESTING_DEBUG_STDOUT          1
#define TESTING_STDOUT_DEBUG          1
#define TESTING_STDERR_DEBUG          1

// **** KOS-specific tests

//#define TESTING_KEYCODE_LOOP          1
//#define TESTING_LOCK_TEST             1
#define TESTING_TCP_TEST              1
#define TESTING_MEMORY_HOG            1
#define TESTING_PING_LOOP             1
//#define TESTING_TIMER_TEST            1

/******************************** sanity checks ********************************/

#if TESTING_SHARED_READYQUEUE
  #if !TESTING_LOADBALANCING
    #warning enabling TESTING_LOADBALANCING for TESTING_SHARED_READYQUEUE
    #define TESTING_LOADBALANCING 1
  #endif
#endif

#if !TESTING_LOADBALANCING
  #if TESTING_PLACEMENT_STAGING
    #warning disabling TESTING_PLACEMENT_STAGING
    #undef TESTING_PLACEMENT_STAGING
  #endif
  #if TESTING_CLUSTER_POLLER_FIBRE
    #warning disabling TESTING_CLUSTER_POLLER_FIBRE
    #undef TESTING_CLUSTER_POLLER_FIBRE
  #endif
#endif
