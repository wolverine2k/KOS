/******************************************************************************
    Copyright (C) Martin Karsten 2015-2019

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _RuntimeImpl_h_
#define _RuntimeImpl_h_ 1

#include "runtime/Runtime.h"
#include "runtime/BlockingSync.h"

#if defined(KERNEL)

extern TimerQueue* kernelTimerQueue;
inline TimerQueue& CurrTimerQueue() { return *kernelTimerQueue; }

#elif defined(__LIBFIBRE__)

#include "libfibre/EventScope.h"

inline void Runtime::notifyTimeout(const Time& t) { CurrEventScope().setTimer(t); }

inline TimerQueue& CurrTimerQueue() { return CurrEventScope().getTimerQueue(); }

#else
#error undefined platform: only KERNEL or __LIBFIBRE__ supported at this time
#endif

#endif /* _RuntimeImpl_h_ */
