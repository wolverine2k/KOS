/******************************************************************************
    Copyright (C) Martin Karsten 2015-2019

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _BlockingSync_h_
#define _BlockingSync_h_ 1

#include "runtime/StackContext.h"
#include "runtime/RuntimeDebug.h"
#include "runtime/RuntimeLock.h"

#include <list>
#include <map>

// RULE: can acquire BlockingQueue lock after Timer lock, but not vice versa!

/***** Timer Handling *****/

class TimerInfo;
template<typename Lock>
class TimerQueueGeneric {
  friend class TimerInfo;
  friend class TimeoutInfo;
  template<typename T, void(*Callback)(T& arg)> friend class CallbackTimer;
  template<typename T> friend class TimeoutBlockingInfo;
public:
  typedef typename std::multimap<Time,TimerInfo*>::iterator iterator;
private:
  Lock lock;
  std::multimap<Time,TimerInfo*> queue;
  TimerStats* stats;
  iterator insert(TimerInfo& ti, const Time& relTimeout, const Time& absTimeout) {
    lock.acquire();
    iterator ret = queue.insert( {absTimeout, &ti} ); // set up timeout
    if (ret == queue.begin()) Runtime::notifyTimeout(relTimeout);
    return ret; // returns with lock held
  }
public:
  TimerQueueGeneric() { stats = new TimerStats(this); }
  iterator insertAbsolute(TimerInfo& ti, const Time& absTimeout, const Time& now) {
    Time relTimeout = absTimeout - now;
    return insert(ti, relTimeout, absTimeout);
  }
  iterator insertRelative(TimerInfo& ti, const Time& relTimeout) {
    Time absTimeout = relTimeout + Runtime::now();
    return insert(ti, relTimeout, absTimeout);
  }
  inline static void sleep(const Time& timeout);
  inline bool checkExpiry(const Time& now, Time& newTime);
};

typedef TimerQueueGeneric<RuntimeLock> TimerQueue;
inline TimerQueue& CurrTimerQueue();

class TimerInfo {
  TimerQueue::iterator titer;
protected:
  TimerQueue& tQueue;
  void prepareRelative(const Time& timeout) {
    titer = tQueue.insertRelative(*this, timeout);
  }
  void prepareAbsolute(const Time& timeout, const Time& now) {
    titer = tQueue.insertAbsolute(*this, timeout, now);
  }
public:
  TimerInfo(TimerQueue& tq = CurrTimerQueue()) : tQueue(tq) {}
  void cancel() {
    ScopedLock<RuntimeLock> al(tQueue.lock);
    tQueue.queue.erase(titer);
  }
  virtual bool checkTimer() { return true; }
  virtual void fireTimer() = 0;
};

template<typename T, void(*Callback)(T& arg)>
class CallbackTimer : public TimerInfo {
  T& arg;
  bool fired;
public:
  CallbackTimer(T& a) : arg(a), fired(true) {}
  void setupRelative(const Time& timeout) {
    fired = false;
    prepareRelative(timeout);
    tQueue.lock.release();
  }
  bool cancel() {
    if (__atomic_test_and_set(&fired, __ATOMIC_RELAXED)) return false;
    TimerInfo::cancel();
    return true;
  }
  virtual bool checkTimer() {
    return !__atomic_test_and_set(&fired, __ATOMIC_RELAXED);
  }
  virtual void fireTimer() {
    Callback(arg);
  }
};

struct ResumeInfo {
  virtual void preResume() {}
};

class SuspendHelper {
  void unlock() {}
  template<typename Lock, typename... Args>
  void unlock(Lock& bl, Args&... ol) {
    bl.release();
    unlock(ol...);
  }
protected:
  void prepareSuspend(StackContext& cs) {
    cs.prepareSuspend(_friend<SuspendHelper>());
  }
  void prepareSuspend(StackContext& cs, ResumeInfo& ri) {
    cs.prepareSuspend(_friend<SuspendHelper>());
    cs.setupResumeRace(ri, _friend<SuspendHelper>());
  }
  template<typename Lock, typename... Args>
  void unlockAndSuspend(StackContext& cs, Lock& l, Args&... ol) {
    unlock(ol...);
    cs.suspend(l, _friend<SuspendHelper>());
  }
};

class TimeoutInfo : public virtual SuspendHelper, public TimerInfo {
public:
  StackContext& stack;
  TimeoutInfo(StackContext& stack = *CurrStack()) : stack(stack) {}
  void suspendRelative(const Time& timeout) {
    prepareSuspend(stack);
    prepareRelative(timeout);
    unlockAndSuspend(stack, tQueue.lock);
  }
  virtual void fireTimer() {
    RuntimeDebugB( "Stack ", FmtHex(&stack), " timed out");
    stack.resume();
  }
};

template<typename Lock>
class BlockingInfo : public virtual SuspendHelper, public ResumeInfo {
protected:
  Lock& lock;
public:
  BlockingInfo(Lock& l) : lock(l) {}
  void suspend(StackContext& stack = *CurrStack()) {
    prepareSuspend(stack);
    unlockAndSuspend(stack, lock);
  }
  void suspend(BlockedStackList& queue, StackContext& stack = *CurrStack()) {
    prepareSuspend(stack, *this);
    queue.push_back(stack);
    unlockAndSuspend(stack, lock);
  }
};

template<typename Lock>
class TimeoutBlockingInfo : public BlockingInfo<Lock>, public TimeoutInfo {
  using BaseBI = BlockingInfo<Lock>;
  bool timedOut;
public:
  TimeoutBlockingInfo(Lock& l, StackContext& stack = *CurrStack()) : BaseBI(l), TimeoutInfo(stack), timedOut(false) {}
  bool suspendAbsolute(BlockedStackList& queue, const Time& timeout, const Time& now) {
    prepareSuspend(stack, *this);
    queue.push_back(stack);
    TimerInfo::prepareAbsolute(timeout, now);
    unlockAndSuspend(stack, BaseBI::lock, tQueue.lock);
    return !timedOut;
  }
  virtual bool checkTimer() {
    return stack.raceResume();
  }
  virtual void fireTimer() {
    timedOut = true;
    BaseBI::lock.acquire();
    BlockedStackList::remove(stack);
    BaseBI::lock.release();
    TimeoutInfo::fireTimer();
  }
  virtual void preResume() {
    timedOut = true;
    TimeoutInfo::cancel();
  }
};

template<typename Lock>
class CallbackBlockingInfo : public BlockingInfo<Lock> {
  funcvoid1_t callback;
  ptr_t arg;
public:
  CallbackBlockingInfo(Lock& l, funcvoid1_t cb, ptr_t a) : BlockingInfo<Lock>(l), callback(cb), arg(a) {}
  virtual void preResume() {
    callback(arg);
  }
};

template<typename Lock>
inline void TimerQueueGeneric<Lock>::sleep(const Time& timeout) {
  TimeoutInfo ti;
  RuntimeDebugB( "Stack ", FmtHex(CurrStack()), " sleep ", timeout);
  ti.suspendRelative(timeout);
}

template<typename Lock>
inline bool TimerQueueGeneric<Lock>::checkExpiry(const Time& now, Time& newTime) {
  bool retcode = false;
  std::list<TimerInfo*> fireList; // defer event locks
  lock.acquire();
  int cnt = 0;
  for (auto it = queue.begin(); it != queue.end(); ) {
    if (it->first > now) {
      retcode = true;
      newTime = it->first - now;
  break;
    }
#if TESTING_ENABLE_STATISTICS
    cnt += 1;
#endif
    TimerInfo* timer = it->second;
    if (timer->checkTimer()) {
      it = queue.erase(it);
      fireList.push_back(timer);
    } else {
      it = next(it);
    }
  }
  stats->events.add(cnt);
  lock.release();
  for (auto f : fireList) f->fireTimer(); // timeout lock released
  return retcode;
}

class BlockingQueue {
  BlockedStackList queue;

  BlockingQueue(const BlockingQueue&) = delete;            // no copy
  BlockingQueue& operator=(const BlockingQueue&) = delete; // no assignment

public:
  BlockingQueue() = default;
  ~BlockingQueue() { GENASSERT(empty()); }
  bool empty() const { return queue.empty(); }

  template<typename Lock>
  void reset(Lock& lock) {
    GENASSERT(lock.test());
    StackContext* s = queue.front();
    while (s != queue.edge()) {
      ResumeInfo* ri = s->raceResume();
      StackContext* ns = BlockedStackList::next(*s);
      if (ri) {
        ri->preResume();
        BlockedStackList::remove(*s);
        RuntimeDebugB( "Stack ", FmtHex(s), " clear/resume from ", FmtHex(&queue));
        s->resume();
      }
      s = ns;
    }
    lock.release();
    while (!empty()) Pause();     // wait for timed out events to disappear
  }

  template<typename Lock>
  bool block(Lock& lock, funcvoid1_t resumeCallback, ptr_t arg) {
    CallbackBlockingInfo<Lock> bi(lock, resumeCallback, arg);
    RuntimeDebugB( "Stack ", FmtHex(CurrStack()), " blocking on ", FmtHex(&queue));
    bi.suspend(queue);
    RuntimeDebugB( "Stack ", FmtHex(CurrStack()), " continuing on ", FmtHex(&queue));
    return true;
  }

  template<typename Lock>
  bool block(Lock& lock, bool wait) {
    if (wait) {
      BlockingInfo<Lock> bi(lock);
      RuntimeDebugB( "Stack ", FmtHex(CurrStack()), " blocking on ", FmtHex(&queue));
      bi.suspend(queue);
      RuntimeDebugB( "Stack ", FmtHex(CurrStack()), " continuing on ", FmtHex(&queue));
      return true;
    }
    lock.release();
    return false;
  }

  template<typename Lock>
  bool block(Lock& lock, const Time& timeout) {
    Time now = Runtime::now();
    if (timeout > now) {
      TimeoutBlockingInfo<Lock> tbi(lock);
      RuntimeDebugB( "Stack ", FmtHex(CurrStack()), " blocking on ", FmtHex(&queue), " timeout ", timeout);
      bool ret = tbi.suspendAbsolute(queue, timeout, now);
      RuntimeDebugB( "Stack ", FmtHex(CurrStack()), " continuing on ", FmtHex(&queue));
      return ret;
    }
    lock.release();
    return false;
  }

  template<typename Lock>
  bool block(Lock& lock) { return block(lock, true); }

  template<bool Enqueue = true>
  StackContext* unblock() {       // not concurrency-safe; better hold lock
    for (StackContext* s = queue.front(); s != queue.edge(); s = BlockedStackList::next(*s)) {
      ResumeInfo* ri = s->raceResume();
      if (ri) {
        ri->preResume();
        BlockedStackList::remove(*s);
        RuntimeDebugB( "Stack ", FmtHex(s), " resume from ", FmtHex(&queue));
        if (Enqueue) s->resume();
        return s;
      }
    }
    return nullptr;
  }
};

template<typename Lock, bool Binary = false, typename BQ = BlockingQueue>
class FifoSemaphore {
protected:
  Lock lock;
  ssize_t counter;
  BQ bq;

  template<typename... Args>
  bool internalP(const Args&... args) {
    // need baton passing: counter unchanged, if blocking fails (timeout)
    if (counter < 1) return bq.block(lock, args...);
    counter -= 1;
    lock.release();
    return true;
  }

public:
  explicit FifoSemaphore(ssize_t c = 0) : counter(c) {}
  // baton passing requires serialization at destruction
  ~FifoSemaphore() { ScopedLock<Lock> sl(lock); }
  bool empty() { return bq.empty(); }
  bool open() { return counter >= 1; }
  ssize_t getValue() { return counter; }

  void reset(ssize_t c = 0) {
    lock.acquire();
    counter = c;
    bq.reset(lock);
  }

  template<typename... Args>
  bool P(const Args&... args) { lock.acquire(); return internalP(args...); }
  bool tryP() { return P(false); }

  template<typename Lock2>
  bool P_unlock(Lock2& l) {
    lock.acquire();
    l.release();
    return internalP(true);
  }

  void P_fake(size_t c = 1) {
    ScopedLock<Lock> al(lock);
    if (Binary) counter = 0;
    else counter -= c;
  }

  template<bool Enqueue = true>
  StackContext* V() {
    ScopedLock<Lock> al(lock);
    StackContext* sc = bq.template unblock<Enqueue>();
    if (sc) return sc;
    if (Binary) counter = 1;
    else counter += 1;
    return nullptr;
  }
};

template<typename Lock, bool OwnerLock = false, typename BQ = BlockingQueue>
class FifoMutex {
  Lock lock;
  StackContext* owner;
  BQ bq;

protected:
  template<typename... Args>
  bool internalAcquire(const Args&... args) {
    StackContext* cs = CurrStack();
    lock.acquire();
    if (!owner) {
      owner = cs;
    } else if (owner == cs) {
      GENASSERT1(OwnerLock, FmtHex(owner));
    } else {
      return bq.block(lock, args...);
    }
    lock.release();
    return true;
  }

public:
  FifoMutex() : owner(nullptr) {}
  // baton passing requires serialization at destruction
  ~FifoMutex() { ScopedLock<Lock> sl(lock); }
  bool test() const { return owner != nullptr; }

  template<typename... Args>
  bool acquire(const Args&... args) { return internalAcquire(args...); }
  bool tryAcquire() { return acquire(false); }

  void release() {
    ScopedLock<Lock> al(lock);
    GENASSERT1(owner == CurrStack(), FmtHex(owner));
    owner = bq.unblock();
  }
};

template<typename Lock, bool OwnerLock = false, typename BQ = BlockingQueue>
class BargingMutex {
  Lock lock;
  StackContext* owner;
  BQ bq;

protected:
  template<typename... Args>
  bool internalAcquire(const Args&... args) {
    StackContext* cs = CurrStack();
    for (;;) {
      lock.acquire();
      if (!owner) break;
      if (owner == cs) { GENASSERT1(OwnerLock, FmtHex(owner)); break; }
      if (!bq.block(lock, args...)) return false;
    }
    owner = cs;
    lock.release();
    return true;
  }

public:
  BargingMutex() : owner(nullptr) {}
  bool test() const { return owner != nullptr; }

  template<typename... Args>
  bool acquire(const Args&... args) { return internalAcquire(args...); }
  bool tryAcquire() { return acquire(false); }

  void release() {
    ScopedLock<Lock> al(lock);
    GENASSERT1(owner == CurrStack(), FmtHex(owner));
    owner = nullptr;
    bq.unblock();
  }
};

template<typename Semaphore, bool OwnerLock, size_t SpinStart, size_t SpinEnd, size_t SpinCount>
class SpinMutex {
  StackContext* owner;
  Semaphore sem;

protected:
  template<typename... Args>
  bool internalAcquire(const Args&... args) {
    StackContext* cs = CurrStack();
    if (OwnerLock && cs == owner) return true;
    GENASSERTN(cs != owner, FmtHex(cs), FmtHex(owner));
    size_t cnt = 0;
    size_t spin = SpinStart;
    for (;;) {
      StackContext* exp = nullptr;
      if (__atomic_compare_exchange_n(&owner, &exp, cs, false, __ATOMIC_SEQ_CST, __ATOMIC_RELAXED)) return true;
      if (cnt < SpinCount) {
        for (size_t i = 0; i < spin; i += 1) Pause();
        if (spin <= SpinEnd) spin += spin;
        else cnt += 1;
      } else {
        if (!sem.P(args...)) return false;
        cnt = 0;
        spin = SpinStart;
      }
    }
  }

public:
  SpinMutex() : owner(nullptr), sem(1) {}
  bool test() const { return owner != nullptr; }

  template<typename... Args>
  bool acquire(const Args&... args) { return internalAcquire(args...); }
  bool tryAcquire() { return acquire(false); }

  void release() {
    GENASSERT1(owner == CurrStack(), FmtHex(owner));
    __atomic_store_n(&owner, nullptr, __ATOMIC_RELAXED); // memory sync via sem.V()
    sem.V();
  }
};

template<typename BaseMutex>
class OwnerMutex : private BaseMutex {
  size_t counter;

public:
  OwnerMutex() : counter(0) {}

  template<typename... Args>
  size_t acquire(const Args&... args) {
    if (BaseMutex::internalAcquire(args...)) return ++counter; else return 0;
  }
  size_t tryAcquire() { return acquire(false); }

  size_t release() {
    if (--counter > 0) return counter;
    BaseMutex::release();
    return 0;
  }
};

template<typename Lock>
#if TESTING_MUTEX_FIFO
class Mutex : public FifoMutex<Lock> {};
#elif TESTING_MUTEX_BARGING
class Mutex : public BargingMutex<Lock> {};
#elif TESTING_MUTEX_SPIN
class Mutex : public SpinMutex<FifoSemaphore<Lock, true>, false, 4, 1024, 16> {};
#else
class Mutex : public SpinMutex<FifoSemaphore<Lock, true>, false, 0, 0, 0> {};
#endif

// simple blocking RW lock: release alternates; new readers block when writer waits -> no starvation
template<typename Lock, typename BQ = BlockingQueue>
class LockRW {
  Lock lock;
  ssize_t state;                    // -1 writer, 0 open, >0 readers
  BQ bqR;
  BQ bqW;

  template<typename... Args>
  bool internalAR(const Args&... args) {
    lock.acquire();
    if (state < 0 || !bqW.empty()) {
      if (!bqR.block(lock, args...)) return false;
      lock.acquire();
      bqR.unblock();                // waiting readers can barge after writer
    }
    state += 1;
    lock.release();
    return true;
  }

  template<typename... Args>
  bool internalAW(const Args&... args) {
    lock.acquire();
    if (state != 0) {
      if (!bqW.block(lock, args...)) return false;
      lock.acquire();
    }
    state -= 1;
    lock.release();
    return true;
  }

public:
  LockRW() : state(0) {}

  template<typename... Args>
  bool acquireRead(const Args&... args) { return internalAR(args...); }
  bool tryAcquireRead() { return acquireRead(false); }

  template<typename... Args>
  bool acquireWrite(const Args&... args) { return internalAW(args...); }
  bool tryAcquireWrite() { return acquireWrite(false); }

  void release() {
    ScopedLock<Lock> al(lock);
    GENASSERT(state != 0);
    if (state > 0) {             // reader leaves; if open -> writer next
      state -= 1;
      if (state > 0) return;
      if (!bqW.unblock()) bqR.unblock();
    } else {                     // writer leaves -> readers next
      GENASSERT(state == -1);
      state += 1;
      if (!bqR.unblock()) bqW.unblock();
    }
  }
};

// simple blocking barrier
template<typename Lock, typename BQ = BlockingQueue>
class Barrier {
  Lock lock;
  size_t target;
  size_t counter;
  BQ bq;
public:
  Barrier(size_t t = 1) : target(t), counter(0) { GENASSERT(t); }
  void reset(size_t t = 1) {
    GENASSERT(t);
    lock.acquire();
    target = t;
    counter = 0;
    bq.reset(lock);
  }
  bool wait() {
    lock.acquire();
    counter += 1;
    if (counter == target) {
      while (bq.unblock());
      counter = 0;
      lock.release();
      return true;
    } else {
      bq.block(lock, true);
      return false;
    }
  }
};

// simple blocking condition variable: assume caller holds lock
template<typename Lock, typename BQ = BlockingQueue>
class Condition {
  BQ bq;
public:
  bool empty() { return bq.empty(); }
  void reset(Lock& lock) { bq.reset(lock); }
  bool wait(Lock& lock) { return bq.block(lock, true); }
  bool wait(Lock& lock, const Time& timeout) { return bq.block(lock, timeout); }
  template<bool Broadcast = false>
  void signal() { while (bq.unblock() && Broadcast); }
};

// cf. condition variable: assume caller to wait/post holds lock
template<typename Lock>
class SynchronizedFlag {

public:
  enum State { Running = 0, Dummy = 1, Posted = 2, Detached = 4 };

protected:
  union {                             // 'waiter' set <-> 'state == Waiting'
    StackContext* waiter;
    State         state;
  };

public:
  static const State Invalid = Dummy; // dummy bit never set (for ManagedArray)
  SynchronizedFlag(State s = Running) : state(s) {}
  void reset()          { state = Running; }
  bool posted()   const { return state == Posted; }
  bool detached() const { return state == Detached; }

  bool wait(Lock& lock) {             // returns false, if detached
    if (state == Running) {
      BlockingInfo<Lock> bi(lock);
      waiter = CurrStack();
      bi.suspend(*waiter);
      lock.acquire();                 // reacquire lock to check state
    }
    if (state == Posted) return true;
    if (state == Detached) return false;
    GENABORT1(FmtHex(state));
  }

  bool post() {                       // returns false, if already detached
    GENASSERT(state != Posted);       // check for spurious posts
    if (state == Detached) return false;
    if (state != Running) waiter->resume();
    state = Posted;
    return true;
  }

  void detach() {                     // returns false, if already posted or detached
    GENASSERT(state != Detached && state != Posted);
    if (state != Running) waiter->resume();
    state = Detached;
  }
};

// cf. condition variable: assume caller to wait/post holds lock
template<typename Runner, typename Result, typename Lock>
class Joinable : public SynchronizedFlag<Lock> {
  using Baseclass = SynchronizedFlag<Lock>;
protected:
  union {
    Runner* runner;
    Result  result;
  };

public:
  Joinable(Runner* t) : runner(t) {}

  bool wait(Lock& bl, Result& r) {
    bool retcode = Baseclass::wait(bl);
    r = retcode ? result : 0; // result is available after returning from wait
    return retcode;
  }

  bool post(Result r) {
    bool retcode = Baseclass::post();
    if (retcode) result = r;  // still holding lock while setting result
    return retcode;
  }

  Runner* getRunner() const { return runner; }
};

template<typename Lock>
class SyncPoint : public SynchronizedFlag<Lock> {
  using Baseclass = SynchronizedFlag<Lock>;
  typedef typename Baseclass::State State;
  Lock lock;
public:
  SyncPoint(State s = Baseclass::Running) : Baseclass(s) {}
  void reset()  { ScopedLock<Lock> al(lock); Baseclass::reset(); }
  bool wait()   { ScopedLock<Lock> al(lock); return Baseclass::wait(lock); }
  bool post()   { ScopedLock<Lock> al(lock); return Baseclass::post(); }
  void detach() { ScopedLock<Lock> al(lock); Baseclass::detach(); }
};

#endif /* _BlockingSync_h_ */
