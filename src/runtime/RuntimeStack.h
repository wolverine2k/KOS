/******************************************************************************
    Copyright (C) Martin Karsten 2015-2019

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _RuntimeStack_h_
#define _RuntimeStack_h_ 1

#if defined(KERNEL)

#include "kernel/Process.h"

inline void RuntimePreStackSwitch(StackContext& currStack, StackContext& nextStack, _friend<StackContext> fs) {
  Thread& currThread = reinterpret_cast<Thread&>(currStack);
  Thread& nextThread = reinterpret_cast<Thread&>(nextStack);
  AddressSpace& currAS = currThread.getAS();
  currAS.preStackSwitch(currThread);
  currAS.switchTo(nextThread.getAS());
  LocalProcessor::setCurrStack(&nextStack, fs);
}

inline void RuntimePostStackSwitch(StackContext& newStack, _friend<StackContext> fs) {
  Thread& newThread = reinterpret_cast<Thread&>(newStack);
  newThread.getAS().postStackSwitch(newThread);
}

inline void RuntimeStackDestroy(StackContext& prevStack, _friend<StackContext> fs) {
  Thread& prevThread = reinterpret_cast<Thread&>(prevStack);
  if (prevThread.getAS().threadTerminated()) {
    Process* p = &reinterpret_cast<Process&>(prevThread.getAS());
    delete p;
  }
  prevThread.destroy(fs);
}

#elif defined(__LIBFIBRE__)

#include "libfibre/Fibre.h"

inline void RuntimePreStackSwitch(StackContext& currStack, StackContext& nextStack, _friend<StackContext> fs) {
  Fibre& currFibre = reinterpret_cast<Fibre&>(currStack);
  Fibre& nextFibre = reinterpret_cast<Fibre&>(nextStack);
  currFibre.deactivate(nextFibre, fs);
  Context::setCurrStack(nextFibre, fs);
}

inline void RuntimePostStackSwitch(StackContext& newStack, _friend<StackContext> fs) {
  Fibre& newFibre = reinterpret_cast<Fibre&>(newStack);
  newFibre.activate(fs);
}

inline void RuntimeStackDestroy(StackContext& prevStack, _friend<StackContext> fs) {
  Fibre& prevFibre = reinterpret_cast<Fibre&>(prevStack);
  prevFibre.destroy(fs);
}

#else
#error undefined platform: only KERNEL or __LIBFIBRE__ supported at this time
#endif

#endif /* _RuntimeStack_h_ */
